#### tech stack
 I used PHP7.0, Mysql and Apache on an Ubuntu machine. 
 You need to add a custom field to your `basic page` called `picture` and machine readable name for this field would be 
  `field_picture`. Also, you need to go to the `blocks` and move `main_menu` to `header` region.  
  Follow these steps one by one to see the desired results: 
  
  1. copy the `bartikdropdown` to `sites/all/themes`. So the path for info file would be: 
   `sites/all/themes/bartikdropdown.info` .
  
  2. create your menu hierarchy. Note that for top level menus you need to provide an image while for the sub-menus you don't need to do so. 
  
  3. Go to `appearance` and select `bartikdropdown` as the default theme.
     
     
If everything goes well, when you switch between sub level 2, images will remain the same unless you choose a different parent menu. 
