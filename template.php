<?php
function bartikdropdown_links__system_main_menu() {return null;}

function bartikdropdown_menu_tree__main_menu($variables) {
  return '<ul id="main-menu-links" class="menu clearfix">'.$variables['tree'].'</ul>';
}
function bartikdropdown_preprocess(&$variables) {
    $menu_item = db_select('menu_links', 'm')
        ->fields('m', array('depth', 'menu_name', 'has_children'))
        ->condition('link_path', $_GET['q'], '=')
        ->execute()
        ->fetchAssoc();
    $variables['menu_item']= $menu_item;
    $active_trail = menu_get_active_trail();
    end($active_trail);
    $parent = prev($active_trail);
    $node = menu_get_object('node', 1, $path =$parent['link_path'] );

    if(!empty($node)){
        $variables['menu_parent']=field_view_field('node', $node, 'field_picture');
    }
    }